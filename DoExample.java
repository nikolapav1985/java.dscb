
/**
*
* DoExample class
*
* Example of do while loop (also equivalent while and for loops).
*
* Print items of array (and check if strings are same)
*
* compile ----- javac DoExample.java
*
* run ----- java DoExample
*
* test environment ----- os lubuntu 16.04 java 1.8.0
*
*/
class DoExample{
    public static void main(String[] args){
        String[] itemsa = {"book","pen","keyboard","ink","plastics"};
        String[] itemsb = {"book","pen","keyboord","ink","plastis"};
        int i=0;

        System.out.println("\n\n ----- begin examples ----- \n\n");

        do{ // check type errors using do while loop
            System.out.print(itemsa[i].toUpperCase());
            System.out.print(" ");
            System.out.print(itemsb[i].toUpperCase());
            System.out.print(" ");
            if(itemsa[i].equals(itemsb[i])){ // spelling is correct
                System.out.println("Correct!\n");
            } else { // spelling is not correct
                System.out.println("Check spelling!\n");
            }
        }while((++i)<itemsa.length);

        System.out.println(" ----- done do while loop ----- \n\n");

        for(i=0;i<itemsa.length;i++){ // check type errors using for loop
            System.out.print(itemsa[i].toUpperCase());
            System.out.print(" ");
            System.out.print(itemsb[i].toUpperCase());
            System.out.print(" ");
            if(itemsa[i].equals(itemsb[i])){ // spelling is correct
                System.out.println("Correct!\n");
            } else { // spelling is not correct
                System.out.println("Check spelling!\n");
            }
        }

        System.out.println(" ----- done for loop ----- \n\n");

        i=0;

        while(i<itemsa.length){ // check type errors using while loop
            System.out.print(itemsa[i].toUpperCase());
            System.out.print(" ");
            System.out.print(itemsb[i].toUpperCase());
            System.out.print(" ");
            if(itemsa[i].equals(itemsb[i])){ // spelling is correct
                System.out.println("Correct!\n");
            } else { // spelling is not correct
                System.out.println("Check spelling!\n");
            }
            i++;
        }

        System.out.println(" ----- done while loop ----- \n\n");
    }
}
