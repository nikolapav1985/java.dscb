
/**
*
* Example class
*
* Example of while loop (also equivalent do while and for loops).
*
* Print items of array (and check if items is plural or singular)
*
* compile ----- javac Example.java
*
* run ----- java Example
*
* test environment ----- os lubuntu 16.04 java 1.8.0
*
*/
class Example{
    public static void main(String[] args){
        String[] itemsa = {"book","pens","keyboard","ink","plastics"};
        int i=0;

        System.out.println("\n\n ----- begin examples ----- \n\n");

        while(i<itemsa.length){ // check plural or singular using while loop
            System.out.print(itemsa[i].toUpperCase());
            System.out.print(" ");
            if(itemsa[i].endsWith("s")){ // plural
                System.out.println("Plural!\n");
            } else { // singular
                System.out.println("Singular!\n");
            }
            i++;
        }

        System.out.println(" ----- done while loop ----- \n\n");

        for(i=0;i<itemsa.length;i++){ // check plural or singular using for loop
            System.out.print(itemsa[i].toUpperCase());
            System.out.print(" ");
            if(itemsa[i].endsWith("s")){ // plural
                System.out.println("Plural!\n");
            } else { // singular
                System.out.println("Singular!\n");
            }
        }

        System.out.println(" ----- done for loop ----- \n\n");

        i=0;

        do{ // check plural or singular using do while loop
            System.out.print(itemsa[i].toUpperCase());
            System.out.print(" ");
            if(itemsa[i].endsWith("s")){ // plural
                System.out.println("Plural!\n");
            } else { // singular
                System.out.println("Singular!\n");
            }
        }while((++i)<itemsa.length);

        System.out.println(" ----- done do while loop ----- \n\n");
    }
}
