/**
*
* ForExample class
*
* Example of for loop (also equivalent while and do while loops).
*
* Print items of array (top 5 ranked rugby teams 2018)
*
* compile ----- javac ForExample.java
*
* run ----- java ForExample
*
* test environment ----- os lubuntu 16.04 java 1.8.0
*
* EXAMPLE OUTPUT
*
* ----- begin examples ----- 
*
*
* 1 NEW ZEALAND
*
* 2 ENGLAND
*
* 3 IRELAND
*
* 4 AUSTRALIA
*
* 5 SCOTLAND
*
* ----- done for loop ----- 
*
*
* 1 NEW ZEALAND
*
* 2 ENGLAND
*
* 3 IRELAND
*
* 4 AUSTRALIA
*
* 5 SCOTLAND
*
* ----- done while loop ----- 
*
*
* 1 NEW ZEALAND
*
* 2 ENGLAND
*
* 3 IRELAND
*
* 4 AUSTRALIA
*
* 5 SCOTLAND
*
* ----- done do while loop ----- 
*
* LOOP SIMILARITIES (FOR, WHILE, DO WHILE)
*
* All loops can perform in same manner and yield same results.
*
* LOOP DIFFERENCES
*
* FOR LOOP - has 3 parameters 
* (initial condition, boolean check and action in form for(initial;boolean expression;action){...}),
* for loop evaluates boolean condition first, then performs given action, then performs a block of code,
* ADVANTAGES - can be used as a loop universally (instead of other loops), all parameters are optional,
* DISADVANTAGES - a bit more complex syntax
*
* WHILE LOOP - has 1 parameter
* (boolean condition in shape of while(boolean expression){...}), the loop checks boolean condition first,
* as a second step it executes a block of code, ADVANTAGES - a simple syntax (only one check of boolean condition),
* condition is not mandatory, DISADVANTAGES - a bit too simplistic (difficult to define initial conditions)
*
* DO WHILE LOOP - has 1 parameter
* (boolean condition in shape of do{...}while(boolean expression)), the loop executes a block of code first,
* checks boolean condition as a second step, ADVANTAGES - a simple syntax (only one check of boolean condition),
* can run code block first (no initial check of boolean condition), DISADVANTAGES - a bit more complex syntax (code block* and condition), runs code block at least once (no initial check of boolean condition)
*
*/
class ForExample{
    public static void main(String[] args){
        String[] teams = {"new zealand","england","ireland","australia","scotland"};
        int[] rank = {1,2,3,4,5};
        int i=0;

        System.out.println("\n\n ----- begin examples ----- \n\n");

        for(;i<teams.length;i++){ // print teams and rank using for loop
            System.out.print(rank[i]);
            System.out.print(" ");
            System.out.print(teams[i].toUpperCase()+"\n\n");
        }

        System.out.println(" ----- done for loop ----- \n\n");

        i=0;

        while(i<teams.length){ // print teams and rank using while loop
            System.out.print(rank[i]);
            System.out.print(" ");
            System.out.print(teams[i].toUpperCase()+"\n\n");
            i++;
        }

        System.out.println(" ----- done while loop ----- \n\n");

        i=0;

        do{ // print teams and rank using do while loop
            System.out.print(rank[i]);
            System.out.print(" ");
            System.out.print(teams[i].toUpperCase()+"\n\n");
        }while((++i)<teams.length);

        System.out.println(" ----- done do while loop ----- \n\n");
    }
}
